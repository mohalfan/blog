@extends('header')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Post</h1>
    </div>

    <div class="section-body">
        <button class="btn btn-success mb-3" onclick="addPost()"><i class="fas fa-plus"></i> Tambah</button>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('sukses'))
        <div class="alert alert-success text-center">
            {{Session::get('sukses')}}
        </div>
        @endif
        @if(Session::has('gagal'))
        <div class="alert alert-danger text-center">
            {{Session::get('gagal')}}
        </div>
        @endif
        <table class="table table-striped">
            <thead>
                <tr class="text-center">
                    <th>No</th>
                    <th>Judul</th>
                    <th>Konten</th>
                    <th>Tanggal</th>
                    <th>Author</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($post as $p)
                <tr class="text-center">
                    <td>{{$loop->iteration}}</td>
                    <td>{{$p->title}}</td>
                    <td>{{$p->content}}</td>
                    <td>{{$p->date}}</td>
                    <td>{{$p->username}}</td>
                    <td>
                        <button class="btn btn-primary" onclick="editpost('{{$p->idpost}}')"><i class="fas fa-edit"></i> Edit</button>
                        <a class="btn btn-danger" style="color: #fff;" onclick="return confirm('Yakin melanjutkan hapus data ? ')" href="{{ route('hapus.post', ['id' => $p->idpost]) }}"><i class="fas fa-edit"></i> Hapus</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
@endsection