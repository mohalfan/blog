@extends('header')
@section('content')
<section class="section">
	<div class="section-header">
		<h1>Dashboard</h1>
	</div>

	<div class="section-body">
		@foreach($post as $p)
		<div class="card" style="padding: 15px;">
			<h3>{{$p->title}}</h3>
			<p>{{$p->content}}</p>
			<span>Tanggal publish : {{date('d-m-Y', strtotime($p->date))}}</span>
		</div>
		@endforeach
	</div>
</section>
@endsection