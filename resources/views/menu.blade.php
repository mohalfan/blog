<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="index.html">Blog</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">BO</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li>
            <a class="nav-link" href="{{ route('dashboard') }}">
                <i class="fas fa-fire"></i> 
                <span>Beranda</span>
            </a>
        </li>
        @if(Auth::user()->role == 'admin')
        <li class="menu-header">Account</li>
        <li>
            <a class="nav-link" href="{{ route('account') }}">
                <i class="fas fa-user"></i> 
                <span>Account</span>
            </a>
        </li>
        @endif
        <li class="menu-header">Post</li>
        <li>
            <a class="nav-link" href="{{ route('post') }}">
                <i class="fas fa-envelope"></i> 
                <span>Post</span>
            </a>
        </li>
    </ul>
</aside>