@extends('header')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Accounts</h1>
    </div>

    <div class="section-body">
        <button class="btn btn-success mb-3" onclick="addAccount()"><i class="fas fa-plus"></i> Tambah</button>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('sukses'))
        <div class="alert alert-success text-center">
            {{Session::get('sukses')}}
        </div>
        @endif
        @if(Session::has('gagal'))
        <div class="alert alert-danger text-center">
            {{Session::get('gagal')}}
        </div>
        @endif
        <table class="table table-striped">
            <thead>
                <tr class="text-center">
                    <th>No</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Role</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($account as $acc)
                <tr class="text-center">
                    <td>{{$loop->iteration}}</td>
                    <td>{{$acc->username}}</td>
                    <td>{{$acc->name}}</td>
                    <td>{{$acc->role}}</td>
                    <td>
                        <button class="btn btn-primary" onclick="editacc('{{$acc->id}}')"><i class="fas fa-edit"></i> Edit</button>
                        <a class="btn btn-danger" style="color: #fff;" onclick="return confirm('Yakin melanjutkan hapus data ? ')" href="{{ route('hapus.account', ['id' => $acc->id]) }}"><i class="fas fa-edit"></i> Hapus</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
@endsection