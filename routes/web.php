<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\PostController;
use App\Models\User;
use App\Models\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function () {
    Route::get('/', function () {
        return view('login');
    })->name('login');
    Route::post('do/login', [AuthController::class, 'doLogin'])->name('doLogin');
});

Route::get('ajax/select/account', [AccountController::class, 'ajaxSelectAcc'])->name('ajax.select.account');
Route::get('ajax/select/post', [PostController::class, 'ajaxSelectPost'])->name('ajax.select.post');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', function () {
        $data['post'] = Post::orderBy('date', 'asc')->get();
        return view('dashboard', $data);
    })->name('dashboard');

    Route::get('account', function () {
        $data['account'] = User::where('id', '<>', Auth::user()->id)->get();
        return view('account', $data);
    })->name('account');
    Route::post('save/account', [AccountController::class, 'save'])->name('save.account');
    Route::post('ubah/account', [AccountController::class, 'ubah'])->name('ubah.account');
    Route::get('hapus/account', [AccountController::class, 'hapus'])->name('hapus.account');

    Route::get('post', function () {
        $data['post'] = Post::where('username', Auth::user()->username)->get();
        return view('post', $data);
    })->name('post');
    Route::post('save/post', [PostController::class, 'save'])->name('save.post');
    Route::post('ubah/post', [PostController::class, 'ubah'])->name('ubah.post');
    Route::get('hapus/post', [PostController::class, 'hapus'])->name('hapus.post');

    Route::get('do/logout', [AuthController::class, 'doLogout'])->name('doLogout');
});
