<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    function doLogin(Request $req){
        $req->validate([
            'uname' => "required",
            'pass' => "required"
        ],[
            'uname.required' => 'Username harus diisi',
            'pass.required' => 'Password harus diisi'
        ]);

        if (Auth::attempt(['username' => $req->uname, 'password' => $req->pass])) {
            return redirect()->intended('dashboard');
        }else{
            return redirect()->back()->with('gagal', 'Username / password salah');
        }
    }

    function doLogout(Request $req){
        if (Auth::check()) {
            Auth::logout();
        }
        return redirect()->route('login')->with('sukses', 'Logout berhasil !');
    }
}
