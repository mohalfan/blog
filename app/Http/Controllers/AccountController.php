<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class AccountController extends Controller
{
    function save(Request $req)
    {
        $req->validate([
            'nama' => 'required',
            'username' => 'required|unique:users',
            'pass' => 'required'
        ], [
            'nama.required' => 'Nama harus diisi',
            'username.required' => 'Username harus diisi',
            'pass.required' => 'Password harus diisi',
            'username.unique' => 'Username sudah digunakan'
        ]);

        try {
            User::create([
                'name' => $req->nama,
                'username' => $req->username,
                'password' => Hash::make($req->pass),
                'role' => 'author'
            ]);

            return redirect('/account')->with('sukses', 'Account berhasil ditambahkan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('gagal', 'Terjadi kesalahan');
        }
    }

    function ubah(Request $req)
    {
        $req->validate([
            'nama' => 'required',
            'username' => 'required|unique:users,username,'. $req->id,
        ], [
            'nama.required' => 'Nama harus diisi',
            'username.required' => 'Username harus diisi',
            'username.unique' => 'Username sudah digunakan'
        ]);

        try {
            if ($req->pass != '') {
                User::where('id', $req->id)->update([
                    'name' => $req->nama,
                    'username' => $req->username,
                    'password' => Hash::make($req->pass),
                ]);
            } else {
                User::where('id', $req->id)->update([
                    'name' => $req->nama,
                    'username' => $req->username,
                ]);
            }

            return redirect('/account')->with('sukses', 'Account berhasil diubah');
        } catch (\Throwable $th) {
            return redirect()->back()->with('gagal', 'Terjadi kesalahan');
        }
    }

    function ajaxSelectAcc(Request $req)
    {
        $data = User::where('id', $req->id)->first();
        return response()->json($data);
    }

    function hapus(Request $req){
        try {
            User::where('id', $req->id)->delete();
            return redirect('/account')->with('sukses', 'Account berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('gagal', 'Terjadi kesalahan');
        }
    }
}
