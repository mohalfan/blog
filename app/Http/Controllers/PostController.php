<?php

namespace App\Http\Controllers;
use App\Models\Post;
use Illuminate\Http\Request;
use Auth;
class PostController extends Controller
{
    function ajaxSelectPost(Request $req){
        $data = Post::where('idpost', $req->id)->first();
        return response()->json($data);
    }

    function save(Request $req){
        $req->validate([
            'title' => 'required',
            'content' => 'required',
        ],[
            'title.required' => 'Judul harus diisi',
            'content.required' => 'Konten harus diisi'
        ]);

        try {
            Post::create([
                'title' => $req->title,
                'content' => $req->content,
                'date' => date('Y-m-d H:i:s', strtotime(now())),
                'username' => Auth::user()->username
            ]);
            return redirect('/post')->with('sukses', 'Post berhasil ditambahkan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('gagal', $th->getMessage());
        }
    }

    function ubah(Request $req){
        $req->validate([
            'title' => 'required',
            'content' => 'required',
        ],[
            'title.required' => 'Judul harus diisi',
            'content.required' => 'Konten harus diisi'
        ]);

        try {
            Post::where('idpost', $req->id)->update([
                'title' => $req->title,
                'content' => $req->content,
            ]);
            return redirect('/post')->with('sukses', 'Post berhasil diubah');
        } catch (\Throwable $th) {
            return redirect()->back()->with('gagal', 'Terjadi kesalahan');
        }
    }

    function hapus(Request $req){
        try {
            Post::where('idpost', $req->id)->delete();
            return redirect('/post')->with('sukses', 'Post berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('gagal', 'Terjadi kesalahan');
        }
    }
}
